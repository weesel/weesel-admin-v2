import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { selectMerchantOrders } from "slices/merchantSlice";
import { Link } from "react-router-dom";

function Order() {
  const orders = useSelector(selectMerchantOrders);
  const [pending, setPending] = useState(0);
  const [completed, setCompleted] = useState(0);

  useEffect(() => {
    if (orders) {
      const totalPending = orders.reduce((t, order) => {
        if (order.status === "pending") {
          return t + 1;
        } else {
          return t;
        }
      }, 0);
      const totalCompleted = orders.reduce((t, order) => {
        if (order.status === "completed") {
          return t + 1;
        } else {
          return t;
        }
      }, 0);

      setPending(totalPending);
      setCompleted(totalCompleted);
    }
  }, [orders]);

  return (
    <div className="merchant-layout-container">
      <div className="merchant-layout-title">Order</div>
      <div>
        <table className="merchant-layout-table">
          <tr className="merchant-layout-table-row">
            <th className="merchant-layout-table-header">ID</th>
            <th className="merchant-layout-table-header">Name</th>
            <th className="merchant-layout-table-header">Address</th>
            <th className="merchant-layout-table-header">Phone Number</th>
            <th className="merchant-layout-table-header">District</th>
            <th className="merchant-layout-table-header">Payment Method</th>
            <th className="merchant-layout-table-header">ABA Transaction ID</th>
            <th className="merchant-layout-table-header">Status</th>
            <th className="mechant-layout-table-header"></th>
          </tr>
          {orders.map((order) => (
            <React.Fragment>
              <tr className="merchant-layout-table-row">
                <td className="merchant-layout-table-data">{order.id}</td>
                <td className="merchant-layout-table-data">{order.name}</td>
                <td className="merchant-layout-table-data">
                  {order.address_1}
                </td>
                <td className="merchant-layout-table-data">
                  {order.phone_number}
                </td>
                <td className="merchant-layout-table-data">{order.district}</td>
                <td className="merchant-layout-table-data">
                  {order.payment_method}
                </td>
                <td className="merchant-layout-table-data">
                  {order.aba_transaction_id}
                </td>
                <td className="merchant-layout-table-data">
                  {order.status.toUpperCase()}
                </td>
                <td className="merchant-layout-table-data">
                  {order.products.map((product) => (
                    <div>
                      {product.name} Quantity:{" "}
                      {product.pivot.order_product_quantity}{" "}
                    </div>
                  ))}
                </td>
              </tr>
            </React.Fragment>
          ))}
        </table>
      </div>
    </div>
  );
}

export default Order;
