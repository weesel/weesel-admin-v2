import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import {
  selectMerchantOrders,
  selectMerchantCounter,
} from "slices/merchantSlice";

function TotalRevenue() {
  const orders = useSelector(selectMerchantOrders);
  const [totalRevenue, setTotalRevenue] = useState(0);
  const refreshCounter = useSelector(selectMerchantCounter);

  useEffect(() => {
    const tr = orders
      .reduce((totalRevenue, order) => {
        let orderTotalPrice = order.products.reduce(
          (totalPrice, product) =>
            totalPrice + product.price * product.pivot.order_product_quantity,
          0
        );
        return totalRevenue + orderTotalPrice;
      }, 0)
      .toFixed(2);
    setTotalRevenue(tr);
  }, [refreshCounter, orders]);

  return (
    <React.Fragment>
      <div className="merchant-layout-title">Total Revenue:</div>
      <div className="merchant-layout-text">${totalRevenue}</div>
    </React.Fragment>
  );
}

export default TotalRevenue;
