import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { selectMerchantCounter } from "slices/merchantSlice";
import { selectAccessToken, selectUser } from "slices/authSlice";
import {
  capitalizeFirstLetter,
  fetchMerchantOrdersByDate,
} from "utils/constants";

function EarningByDate() {
  const [totalEarnings, setTotalEarnings] = useState(0);
  const [orders, setOrders] = useState([]);
  const refreshCounter = useSelector(selectMerchantCounter);
  const [date, setDate] = useState("today");

  const access_token = useSelector(selectAccessToken);
  const user = useSelector(selectUser);
  useEffect(() => {
    if (user) {
      fetchMerchantOrdersByDate(access_token, user.id, date)
        .then((orders) => setOrders(orders))
        .catch((error) => console.log(error.response));
    }
  }, [user]);

  useEffect(() => {
    const tr = orders
      .reduce((totalRevenue, order) => {
        let orderTotalPrice = order.products.reduce(
          (totalPrice, product) =>
            totalPrice + product.price * product.pivot.order_product_quantity,
          0
        );
        return totalRevenue + orderTotalPrice;
      }, 0)
      .toFixed(2);
    setTotalEarnings(tr);
  }, [orders, refreshCounter]);

  return (
    <React.Fragment>
      <div className="merchant-layout-title">
        Earning {capitalizeFirstLetter(date)}:
      </div>
      <div className="merchant-layout-text">${totalEarnings}</div>
    </React.Fragment>
  );
}

export default EarningByDate;
