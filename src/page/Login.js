import React, { useEffect, useState } from "react";
import { Form, Button, Container, Message } from "semantic-ui-react";
import { useDispatch, useSelector } from "react-redux";
import {
  loginWithRedirect,
  selectAuthErrorMessage,
  clearErrorMessage,
} from "slices/authSlice";
import { useHistory } from "react-router-dom";

function Login() {
  const dispatch = useDispatch();
  const history = useHistory();

  const errorMessage = useSelector(selectAuthErrorMessage);
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  const onSubmitHandler = () =>
    dispatch(
      loginWithRedirect({
        loginCredentials: formData,
        redirectCallback: () => history.push("/merchant/dashboard"),
      })
    );

  const onChangeHandler = (_, { name, value }) =>
    setFormData((prevState) => {
      return {
        ...prevState,
        [name]: value,
      };
    });

  useEffect(() => {
    var clearInterval;
    if (errorMessage.header.length > 0 && errorMessage.content.length > 0) {
      clearInterval = setTimeout(() => {
        dispatch(clearErrorMessage());
      }, 4000);
    }

    return () => clearTimeout(clearInterval);
  }, [errorMessage]);

  return (
    <Container
      style={{
        display: "flex",
        alignItems: "center",
        minHeight: "100vh",
        flexDirection: "column",
        justifyContent: "center",
      }}
    >
      {errorMessage.header.length > 0 && errorMessage.content.length > 0 && (
        <Message
          style={{ width: "100%" }}
          error
          header={errorMessage.header}
          content={errorMessage.content}
        />
      )}
      <Form onSubmit={onSubmitHandler} style={{ width: "100%" }}>
        <Form.Field required>
          <Form.Input
            required
            label="Email: "
            value={formData.email}
            name="email"
            onChange={onChangeHandler}
            placeholder="Email"
          />
        </Form.Field>
        <Form.Field required>
          <Form.Input
            required
            label="Password: "
            type="password"
            value={formData.password}
            name="password"
            onChange={onChangeHandler}
            placeholder="Password"
          />
        </Form.Field>
        <Button type="submit">Submit</Button>
      </Form>
    </Container>
  );
}

export default Login;
