import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { selectAccessToken, selectUser } from "slices/authSlice";
import { fetchMerchantOrders } from "utils/constants";
export default () => {
  const [orders, setOrders] = useState([]);
  const access_token = useSelector(selectAccessToken);
  const user = useSelector(selectUser);
  useEffect(() => {
    if (user) {
      fetchMerchantOrders(access_token, user.id)
        .then((orders) => setOrders(orders))
        .catch((error) => console.log(error.response));
    }
  }, [user]);
  return (
    <div>
      <h1>Merchant Order Page</h1>
      <table>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Address</th>
          <th>Phone Number</th>
          <th>District</th>
          <th>Payment Method</th>
          <th>ABA Transaction ID</th>
          <th>Status</th>
        </tr>
        {orders.map((order) => (
          <tr>
            <td>{order.id}</td>
            <td>{order.name}</td>
            <td>{order.address_1}</td>
            <td>{order.phone_number}</td>
            <td>{order.district}</td>
            <td>{order.payment_method}</td>
            <td>{order.aba_transaction_id}</td>
            <td>{order.status.toUpperCase()}</td>
          </tr>
        ))}
      </table>
    </div>
  );
};
