import React, { useEffect, useState } from "react";
import { useParams, useLocation, Link } from "react-router-dom";
import axios from "axios";
import queryString from "query-string";

import ProductCard from "components/Product/ProductCard";
import Rating from "components/Product/Rating";
import "css/Product.css";
import { useSelector } from "react-redux";
import { selectUser } from "slices/authSlice";
import CreateProduct from "./CreateProduct/CreateProduct";

const fetchCategoryProducts = async (id, queryParams) => {
  return axios
    .get(`/categories/${id}/products`, { params: queryParams })
    .then((response) => response.data.data)
    .catch((error) => error);
};

const fetchCategory = async (id) => {
  return axios
    .get(`/categories/${id}`)
    .then((response) => response.data.data)
    .catch((error) => error);
};

export default () => {
  const { id } = useParams();
  const user = useSelector(selectUser);
  const [category, setCategory] = useState({});
  const [products, setProducts] = useState([]);
  const { search, pathname } = useLocation();
  const query = queryString.parse(search);
  const subcategory_id =
    Object.keys(query) > 0 && query.subcategory ? query.subcategory : null;
  const [totalPage, setTotalPage] = useState(0);

  /**
   * filter state
   */
  const [categoryFilter, setCategoryFilter] = useState([
    parseInt(subcategory_id || id, 10),
  ]);
  const [ratingFilter, setRatingFilter] = useState("1");
  const [priceFilter, setPriceFilter] = useState("0");
  const [sortBy, setSortBy] = useState("created_at");
  // query params
  const [queryParams, setQueryParams] = useState({
    limit: 12,
    categories: categoryFilter.join(","),
    price: priceFilter,
    rating: ratingFilter,
    page: query.page,
    sort_by: sortBy,
    seller_id: user.id,
  });

  /**
   * filter category state and functions
   */
  const resetCategoryFilter = (e) => setCategoryFilter([parseInt(id, 10)]);
  const categoryFilterHandler = (e) => {
    const newCategoryFilter = categoryFilter;
    // make sure to remove category parent id if exist
    if (newCategoryFilter.includes(parseInt(id, 10)))
      newCategoryFilter.splice(categoryFilter.indexOf(parseInt(id, 10)), 1);

    // checking and unchecking logic
    if (e.target.checked) {
      newCategoryFilter.push(parseInt(e.target.value, 10));
    } else {
      newCategoryFilter.splice(
        categoryFilter.indexOf(parseInt(e.target.value, 10)),
        1
      );
    }
    // update the filtered category state
    setCategoryFilter([...newCategoryFilter]);
  };

  /**
   * filter price state and functions
   */
  const priceFilterHandler = (e) => {
    if (e.target.checked) {
      setPriceFilter(e.target.value);
    } else {
      setPriceFilter("");
    }
  };

  const ratingFilterHandler = (e) => {
    if (e.target.checked) {
      setRatingFilter(e.target.value);
    } else {
      setRatingFilter("");
    }
  };

  /**
   * filtering products based on category
   * set query params each time category filter changes
   */
  useEffect(() => {
    // category filter list contain children id other than parent id
    if (categoryFilter.includes(undefined) || categoryFilter.length === 0) {
      // if nothing is in the category filter list reset it
      resetCategoryFilter();
    }
    setQueryParams({ ...queryParams, categories: categoryFilter.join(",") });
  }, [categoryFilter]);

  /**
   * filtering products based on price
   */
  useEffect(() => {
    if (priceFilter) {
      setQueryParams({ ...queryParams, price: priceFilter });
    }
  }, [priceFilter]);

  /**
   * filtering products based on rating
   */
  useEffect(() => {
    if (ratingFilter) {
      setQueryParams({ ...queryParams, rating: ratingFilter });
    }
  }, [ratingFilter]);
  /**
   * sort by
   */
  useEffect(() => {
    if (sortBy) {
      setQueryParams({ ...queryParams, sort_by: sortBy });
    }
  }, [sortBy]);

  /**
   * interactive query
   * fire filter
   */

  useEffect(() => {
    if (!queryParams["seller_id"] && user) {
      setQueryParams({ ...queryParams, seller_id: user.id });
    }
  }, [user]);

  useEffect(() => {
    fetchCategoryProducts(id, queryParams)
      .then((products) => {
        setProducts(products.data);
        setTotalPage(products.last_page);
      })
      .catch((error) => console.log(error));
  }, [queryParams]);

  useEffect(() => {
    fetchCategory(id)
      .then((category) => {
        console.log(category);
        setCategory({ ...category });
      })
      .catch((error) => error);
  }, []);

  return (
    <div className="categoryProduct">
      <div className="categoryProduct-header">
        <div className="categoryProduct-header-title">
          {category.id && category.name}
        </div>
        <div className="categoryProduct-header-breadcrumbs">
          <CreateProduct />
        </div>
      </div>
      <div className="categoryProduct-content">
        <div className="categoryProduct-left">
          <div className="categoryProduct-category-filter">
            <span>Category</span>
            <ul className="categoryProduct-category-filter-list">
              {category.id && (
                <React.Fragment>
                  <li>
                    <input
                      type="checkbox"
                      name="all"
                      value={category.id}
                      id={`category-${category.id}`}
                      onChange={resetCategoryFilter}
                      checked={categoryFilter.includes(category.id)}
                    />
                    <label htmlFor={`category-${category.id}`}>All</label>
                  </li>
                  {category.children &&
                    category.children.map((child) => (
                      <li key={child.id}>
                        <input
                          type="checkbox"
                          name="all"
                          value={child.id}
                          id={`category-${child.id}`}
                          onChange={categoryFilterHandler}
                          checked={categoryFilter.includes(child.id)}
                        />
                        <label htmlFor={`category-${child.id}`}>
                          {child.name}
                        </label>
                      </li>
                    ))}
                </React.Fragment>
              )}
            </ul>
          </div>
          <div className="categoryProduct-price-filter">
            <span>Price</span>
            <ul className="categoryProduct-price-filter-list">
              {category.id && (
                <React.Fragment>
                  <li>
                    <input
                      type="checkbox"
                      name="all"
                      value={"0"}
                      id={`price-all`}
                      onChange={priceFilterHandler}
                      checked={priceFilter === "0"}
                    />
                    <label htmlFor={`price-all`}>All Price</label>
                  </li>
                  {priceFilterOptions.map((option) => (
                    <li key={option.key}>
                      <input
                        type="checkbox"
                        name="all"
                        value={option.value}
                        id={`price-${option.key}`}
                        onChange={priceFilterHandler}
                        checked={priceFilter === option.value}
                      />
                      <label htmlFor={`price-${option.key}`}>
                        {option.label}
                      </label>
                    </li>
                  ))}
                </React.Fragment>
              )}
            </ul>
          </div>
          <div className="categoryProduct-rating-filter">
            <span>Ratings</span>
            <ul className="categoryProduct-rating-filter-list">
              {category.id && (
                <React.Fragment>
                  {ratingFilterOptions.map((option) => (
                    <li
                      key={option.key}
                      style={{ display: "flex", alignItems: "center" }}
                    >
                      <input
                        type="checkbox"
                        name="all"
                        value={option.value}
                        id={`price-${option.key}`}
                        onChange={ratingFilterHandler}
                        checked={ratingFilter === option.value}
                      />
                      <label
                        style={{ display: "flex", alignItems: "center" }}
                        htmlFor={`price-${option.key}`}
                      >
                        <Rating rating={option.value} />& Up
                      </label>
                    </li>
                  ))}
                </React.Fragment>
              )}
            </ul>
          </div>
        </div>
        <div className="categoryProduct-right">
          <div className="categoryProduct-display-options">
            <div className="categoryProduct-layout">
              <i className="fas fa-th-large"></i>
              <i className="fas fa-th-list"></i>
            </div>
            <div style={{ display: "flex" }}>
              <div className="categoryProduct-sortby">
                <label htmlFor="sort-by">Sort By: </label>
                <select
                  name="sort-by"
                  id="sort-by"
                  onChange={(e) => setSortBy(e.target.value)}
                >
                  <option value="created_at">Latest</option>
                  <option value="name">Alphabetical (A-Z) Order</option>
                  <option value="price-l-h">Price Low to High</option>
                  <option value="price-h-l">Price High to Low</option>
                </select>
              </div>
              <div className="categoryProduct-show">
                <label htmlFor="show">Show: </label>
                <select name="show" id="show">
                  <option value="">12</option>
                </select>
              </div>
            </div>
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              flexDirection: "column",
            }}
          >
            <div className="categoryProduct-product-list">
              {products.map((product) => (
                <Link to={`/product/${product.id}`}>
                  <ProductCard
                    title={product.name}
                    image={product.images[0].url}
                    rating={product.rating}
                    price={product.price}
                  />
                </Link>
              ))}
            </div>
            <div className="categoryProduct-pagination-cursor">
              {totalPage > 0 && parseInt(query.page, 10) < 9 ? (
                Array.from(Array(Math.min(9, totalPage)).keys()).map(
                  (number) => (
                    <Link
                      style={{
                        pointerEvents:
                          parseInt(query.page, 10) === number + 1
                            ? "none"
                            : "all",
                      }}
                      to={{
                        pathname: pathname,
                        search: `?page=${number + 1}`,
                      }}
                    >
                      {number + 1}
                    </Link>
                  )
                )
              ) : (
                <React.Fragment>
                  {Array.from(Array(Math.min(3, totalPage)).keys()).map(
                    (number) => (
                      <Link
                        to={{
                          pathname: pathname,
                          search: `?page=${
                            parseInt(query.page, 10) - (3 - number)
                          }`,
                        }}
                      >
                        {parseInt(query.page, 10) - (3 - number)}
                      </Link>
                    )
                  )}
                  {
                    <Link
                      style={{
                        pointerEvents: "none",
                      }}
                    >
                      {query.page}
                    </Link>
                  }
                  {Array.from(
                    Array(query.page - totalPage > -1 ? 0 : 3).keys()
                  ).map((_, index) => (
                    <Link
                      to={{
                        pathname: pathname,
                        search: `?page=${parseInt(query.page, 10) + index + 1}`,
                      }}
                    >
                      {parseInt(query.page, 10) + index + 1}
                    </Link>
                  ))}
                </React.Fragment>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const priceFilterOptions = [
  {
    key: "0-1",
    label: "0$ to 1$",
    value: "0,1",
  },
  {
    key: "1-5",
    label: "1$ to 5$",
    value: "1,5",
  },
  {
    key: "5-10",
    label: "5$ to 10$",
    value: "5,10",
  },
  {
    key: "10-20",
    label: "10$ to 20$",
    value: "10,20",
  },
  {
    key: "20-30",
    label: "20$ to 30$",
    value: "20,30",
  },
  {
    key: "30+",
    label: "30$ or more",
    value: "30",
  },
];

const ratingFilterOptions = [
  {
    value: "4",
    key: "4",
  },
  {
    value: "3",
    key: "3",
  },
  {
    value: "2",
    key: "2",
  },
  {
    value: "1",
    key: "1",
  },
];
