import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Icon } from "semantic-ui-react";
import { selectCategories, setCategories } from "../../../slices/productSlice";

const fetchCategories = async () => {
  return axios
    .get("/categories")
    .then((response) => response.data.data)
    .catch((error) => error);
};

const toggleExpandIcon = (element) => {
  if (element.classList.contains("right")) {
    element.classList.remove("right");
    element.classList.add("down");
  } else {
    element.classList.remove("down");
    element.classList.add("right");
  }
};

const toggleChildren = (e) => {
  // might change
  toggleExpandIcon(e.target);
  // i ->  div -> nextSibling
  e.target.parentElement.nextSibling?.classList.toggle("hide");
};

export default ({
  selectedCategories,
  addSelectedCategories,
  removeSelectedCategories,
}) => {
  const categories = useSelector(selectCategories);
  const tree = useRef();
  const dispatch = useDispatch();
  useEffect(() => {
    fetchCategories()
      .then((categories) => dispatch(setCategories(categories)))
      .catch((error) => console.log(error));
    if (selectedCategories.length > 0) {
      console.log(selectedCategories);
      Array.from(
        tree.current.querySelectorAll('input[type="checkbox"]')
      ).forEach((input) => {
        if (selectedCategories.indexOf(input.getAttribute("data-id")) !== -1) {
          input.checked = true;
        }
      });
    }
  }, []);

  const toggleCheckAll = (e) => {
    // if parent is checked
    // capture parent id
    const parentId = e.target.getAttribute("data-id");
    e.target.checked
      ? addSelectedCategories(parentId)
      : removeSelectedCategories(parentId);
    // checking all the children
    Array.from(tree.current.querySelectorAll('input[type="checkbox"]')).forEach(
      (child) => {
        const childParentId = child.getAttribute("data-parent-id");
        const childId = child.getAttribute("data-id");
        if (childParentId === parentId) {
          child.checked = !child.checked;
          // capture children id
          child.checked
            ? addSelectedCategories(childId)
            : removeSelectedCategories(childId);
        }
      }
    );
    console.log(selectedCategories);
  };

  const implicitParentCheck = (e) => {
    // children is checked
    const childParentId = e.target.getAttribute("data-parent-id");
    const childId = e.target.getAttribute("data-id");
    e.target.checked
      ? addSelectedCategories(childId)
      : removeSelectedCategories(childId);
    Array.from(tree.current.querySelectorAll('input[type="checkbox"]')).forEach(
      (parent) => {
        const parentId = parent.getAttribute("data-id");
        if (parentId === childParentId) {
          // when checking
          if (e.target.checked) {
            parent.checked = true;
            // if the parent id does not exist in current array
            if (selectedCategories.indexOf(parentId) === -1)
              addSelectedCategories(parentId);
          }
          // when unchecking
          else {
            // checked whether or not there are any other siblings that is checked (same parent-id)
            let noSiblingsChecked = true;
            Array.from(
              document.querySelectorAll('input[type="checkbox"]')
            ).forEach((sibling) => {
              // if the sibling is not equal to the unchecked item itself
              if (
                sibling !== parent &&
                sibling.getAttribute("data-parent-id") === childParentId
              ) {
                // if something is checked
                if (sibling.checked) {
                  noSiblingsChecked = false;
                  return;
                }
              }
            });
            if (noSiblingsChecked) {
              parent.checked = false;
              removeSelectedCategories(childParentId);
            }
          }
        }
      }
    );
  };

  const treeView = (categories) => {
    return (
      <ul ref={tree}>
        {categories.map((category) => (
          <li key={category.id}>
            <div className="row">
              <Icon
                name="chevron down"
                className="clickable"
                onClick={toggleChildren}
              />
              <input
                className="mx-1"
                onClick={toggleCheckAll}
                data-id={category.id}
                data-parent-id={0}
                type="checkbox"
              />
              {category.name}
            </div>
            {category.children.length > 0 && (
              <ul>
                {category.children.map((categoryChild) => (
                  <li className="row" key={categoryChild.id}>
                    <input
                      className="mx-1"
                      data-id={categoryChild.id}
                      data-parent-id={category.id}
                      onClick={implicitParentCheck}
                      type="checkbox"
                    />
                    {categoryChild.name}
                  </li>
                ))}
              </ul>
            )}
          </li>
        ))}
      </ul>
    );
  };
  return (
    <>
      {categories && categories.length > 0 ? (
        treeView(categories)
      ) : (
        <span>No Categories Exist</span>
      )}
    </>
  );
};
