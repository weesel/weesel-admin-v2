import React from "react";
import { Dropdown } from "semantic-ui-react";

function ChooseStatus(props) {
  const status = [
    {
      key: "available",
      text: "Available",
      value: "available",
    },
    {
      key: "unavailable",
      text: "Unavailable",
      value: "unavailable",
    },
  ];
  return (
    <Dropdown
      placeholder="Seller Name"
      fluid
      value={props.value}
      search
      selection
      onChange={props.onChange}
      options={status || []}
    />
  );
}

export default ChooseStatus;
