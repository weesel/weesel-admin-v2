import axios from "axios";
import nProgress from "nprogress";
import React, { useRef, useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  Container,
  Divider,
  Form,
  Header,
  Icon,
  Modal,
  Checkbox,
} from "semantic-ui-react";
import { refreshProduct } from "../../../slices/productSlice";
import SelectCategory from "../SelectCategory";
import ChooseSeller from "./ChooseSeller";
import ChooseStatus from "./ChooseStatus";
import { selectUser } from "slices/authSlice";
import { useParams } from "react-router-dom";
import { fetchCategoryWithChildren } from "utils/constants";
import { selectAccessToken } from "slices/authSlice";

function CreateProduct() {
  const user = useSelector(selectUser);
  const { id } = useParams();
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const access_token = useSelector(selectAccessToken);
  const [quantity, setQuantity] = useState("");
  const [sellerId, setSellerId] = useState("");
  const [status, setStatus] = useState("");
  const [images, setImages] = useState(null);
  const dispatch = useDispatch();
  const [selectedCategories, setSelectedCategories] = useState([parseInt(id)]);
  const [open, setOpen] = useState(false);
  const [price, setPrice] = useState(0);
  const [discount, setDiscount] = useState(0);

  const fileInput = useRef();
  const resetForm = () => {
    setName("");
    setDescription("");
    setQuantity("");
    setSellerId("");
    setStatus("");
    setSelectedCategories([]);
    setImages(null);
    fileInput.current.value = "";
  };

  const [categories, setCategories] = useState([]);
  const selectedCategoriesHandler = (e, { value }) => {
    setSelectedCategories([id, value]);
  };

  useEffect(() => {
    fetchCategoryWithChildren(id).then((category) =>
      setCategories(category.children)
    );
  }, []);

  const submitHandler = () => {
    const formData = new FormData();
    formData.append("name", name);
    formData.append("description", description || "n/a");
    formData.append("quantity", parseInt(quantity));
    formData.append("status", status);
    formData.append("discount", parseInt(discount));
    formData.append("price", parseInt(price));
    selectedCategories.map((category) =>
      formData.append("category_ids[]", parseInt(category))
    );
    images?.map((image) => formData.append("images[]", image));
    nProgress.start();
    axios
      .post("/products", formData, {
        headers: {
          Authorization: `Bearer ${access_token}`,
        },
      })
      .then(() => {
        dispatch(refreshProduct());
        nProgress.done();
      })
      .catch((error) => console.log(error));
  };

  return (
    <Modal
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Button>Create Product</Button>}
    >
      <Modal.Content>
        <Container>
          <Divider horizontal>
            <Header as="h4">
              <Icon name="plus cart" />
              Create Product
            </Header>
          </Divider>
          <Form onSubmit={submitHandler}>
            <Form.Field>
              <Form.Field>
                <Form.Input
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  label="Product Name"
                  placeholder="Product Name"
                />
              </Form.Field>
            </Form.Field>
            <Form.Field>
              <Form.TextArea
                label="Description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                placeholder="Tell us about your product"
              />
            </Form.Field>
            <Form.Field>
              <Form.Input
                value={discount}
                onChange={(e) => setDiscount(e.target.value)}
                label="Discount"
                placeholder="Discount"
              />
            </Form.Field>
            <Form.Field>
              <Form.Input
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                label="Price"
                placeholder="Price"
              />
            </Form.Field>
            <Form.Field>
              <Form.Input
                value={quantity}
                onChange={(e) => setQuantity(e.target.value)}
                label="Quantity"
                placeholder="Quantity"
              />
            </Form.Field>
            <Form.Field>
              <label>Images</label>
              <input
                type="file"
                name="images[]"
                multiple
                // required
                ref={fileInput}
                onChange={(e) => setImages(Array.from(e.target.files))}
              />
            </Form.Field>
            <Form.Field>
              <label>Choose Availability</label>
              <ChooseStatus
                value={status}
                onChange={(e, { value }) => setStatus(value)}
              />
            </Form.Field>
            <Form.Field>
              <label>Choose Category</label>
              {categories &&
                categories.map((category) => (
                  <Checkbox
                    radio
                    label={category.name}
                    name="checkboxRadioGroup"
                    value={category.id}
                    checked={selectedCategories.includes(category.id)}
                    onChange={selectedCategoriesHandler}
                  />
                ))}
            </Form.Field>
          </Form>
        </Container>
      </Modal.Content>
      <Modal.Actions>
        <Button color="black" onClick={() => setOpen(false)}>
          Cancel
        </Button>
        <Button
          content="Create"
          labelPosition="right"
          icon="checkmark"
          onClick={() => {
            submitHandler();
            setOpen(false);
          }}
          positive
        />
      </Modal.Actions>
    </Modal>
  );
}

export default CreateProduct;
