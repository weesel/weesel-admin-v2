import axios from "axios";
import React, { useEffect, useState } from "react";
import { Dropdown } from "semantic-ui-react";
import { semanticUI__optionsFormatter } from "utils/constants";

const fetchSeller = () => {
  return axios
    .get("/users", {
      params: {
        role: "seller",
      },
    })
    .then((response) => response.data.data)
    .catch((error) => error);
};

function ChooseSeller(props) {
  const [sellers, setSellers] = useState();
  useEffect(() => {
    fetchSeller()
      .then((sellers) => {
        setSellers(semanticUI__optionsFormatter("name", "name", "id", sellers));
      })
      .catch((error) => console.log(error));
  }, []);
  return (
    <Dropdown
      placeholder="Seller Name"
      fluid
      value={props.value}
      search
      selection
      onChange={props.onChange}
      options={sellers || []}
    />
  );
}

export default ChooseSeller;
