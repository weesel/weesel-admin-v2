import axios from "axios";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Button, Icon, Image, Item, Label, Loader } from "semantic-ui-react";
import { selectRefreshCounter } from "../../../slices/productSlice";
// import EditProduct from "../../../components/Edit";
import EditProduct from "../EditProduct";
import ConfigMenu from "../../../components/ConfigMenu/ConfigMenu";
import DeleteProduct from "../DeleteProduct/DeleteProduct";

const fetchCategoryProducts = (categoryId) => {
  return axios
    .get(`/categories/${categoryId}/products`)
    .then((response) => response.data.data)
    .catch((error) => error);
};

function CategoryProduct({ categoryId }) {
  const [products, setProducts] = useState([]);
  const refreshCounter = useSelector(selectRefreshCounter);
  useEffect(() => {
    fetchCategoryProducts(categoryId)
      .then((products) => setProducts(products))
      .catch((error) => console.log(error));
  }, [categoryId, refreshCounter]);
  return (
    <Item.Group divided>
      {products &&
        products.map((product) => (
          <Item>
            {product.images.length > 0 && (
              <Item.Image src={product.images[0].url} />
            )}
            <Item.Content>
              <Item.Header>{product.name}</Item.Header>
              <Item.Meta>
                <span>Price: {product.price}</span>
                <span>Rating: {product.rating}</span>
              </Item.Meta>
              <Item.Description>{product.description}</Item.Description>
              <Item.Extra
                style={{ display: "flex", justifyContent: "flex-end" }}
              >
                <div>
                  {product.categories.map((category) => (
                    <Label>{category.name}</Label>
                  ))}
                </div>
                <ConfigMenu
                  id={product.id}
                  editComponent={<EditProduct />}
                  deleteComponent={<DeleteProduct />}
                />
              </Item.Extra>
            </Item.Content>
          </Item>
        ))}
    </Item.Group>
  );
}

export default CategoryProduct;
