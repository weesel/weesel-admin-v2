import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Grid, Menu, Segment } from "semantic-ui-react";
import { selectCategories } from "../../../slices/productSlice";
import CategoryProduct from "./CategoryProduct";

function VerticleMenu() {
  const [activeItem, setActiveItem] = useState(3);
  const categories = useSelector(selectCategories);
  const handleItemClick = (e, { index }) => setActiveItem(index);

  return (
    <>
      {categories.length > 0 ? (
        <Grid>
          <Grid.Column width={4}>
            <Menu fluid vertical tabular>
              {categories.map((category) => (
                <Menu.Item
                  key={category.id}
                  name={category.name}
                  active={activeItem === category.id}
                  index={category.id}
                  onClick={handleItemClick}
                />
              ))}
            </Menu>
          </Grid.Column>
          <Grid.Column stretched width={12}>
            <CategoryProduct categoryId={activeItem} />
          </Grid.Column>
        </Grid>
      ) : (
        <span>No Categories Exists</span>
      )}
    </>
  );
}

export default VerticleMenu;
