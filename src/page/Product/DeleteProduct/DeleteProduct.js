import axios from "axios";
import React from "react";
import { useSelector } from "react-redux";
import { Button, Modal } from "semantic-ui-react";
import { selectConfigId } from "../../../slices/configMenuSlice";
function DeleteProduct() {
  const configId = useSelector(selectConfigId);

  const deleteProduct = async () => {
    axios
      .delete(`/products/${configId}`)
      .then(() => window.location.reload())
      .catch((error) => console.log(error));
  };

  return (
    <>
      <Modal.Header>Delete Product</Modal.Header>
      <Modal.Content>
        <p>Are you sure you want to delete this product?</p>
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={() => deleteProduct()} positive>
          Confirm Delete
        </Button>
      </Modal.Actions>
    </>
  );
}

export default DeleteProduct;
