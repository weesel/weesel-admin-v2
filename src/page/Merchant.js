import React, { useEffect } from "react";
import "css/merchant.css";
import CategoryList from "components/CategoryList";
import { fetchMerchantOrders } from "utils/constants";
import { useDispatch, useSelector } from "react-redux";
import { setMerchantOrders } from "slices/merchantSlice";
import TotalRevenue from "./Merchant/TotalRevenue";
import { selectAccessToken, selectUser } from "slices/authSlice";
import EarningByDate from "./Merchant/EarningByDate";
import Order from "./Merchant/Order";
import Nav from "components/Nav";

function Merchant() {
  const dispatch = useDispatch();
  const access_token = useSelector(selectAccessToken);
  const user = useSelector(selectUser);
  useEffect(() => {
    if (user) {
      fetchMerchantOrders(access_token, user.id)
        .then((orders) => dispatch(setMerchantOrders(orders)))
        .catch((error) => console.log(error.response));
    }
    console.log(user);
  }, [user]);

  return (
    <React.Fragment>
      <div className="merchant">
        <div className="merchant-layout">
          <div className="merchant-layout-left">
            <div className="merchant-layout-title">Category</div>
            <CategoryList />
          </div>
          <div className="merchant-layout-right">
            <div className="merchant-layout-col-2">
              <div className="merchant-layout-col-2a">
                <TotalRevenue />
              </div>
              <div className="merchant-layout-col-2b">
                <EarningByDate />
              </div>
            </div>
            <Order />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default Merchant;
