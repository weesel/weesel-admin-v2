import React, { useEffect } from "react";
import {
  Route,
  Switch,
  useRouteMatch,
  Link,
  useHistory,
} from "react-router-dom";
import Home from "./Home";
import Product from "./Product";

import Category from "./Category";
import {
  selectUser,
  logoutWithRedirect,
  selectAccessToken,
} from "slices/authSlice";
import { useSelector, useDispatch } from "react-redux";
import { Button } from "semantic-ui-react";

function Admin() {
  const user = useSelector(selectUser);
  const access_token = useSelector(selectAccessToken);
  const dispatch = useDispatch();
  const history = useHistory();
  useEffect(() => {
    if (user) {
      console.log(user);
    }
  }, [user]);
  return (
    <div>
      {user && user.role.name === "admin_user" && (
        <React.Fragment>
          <header>
            <h1>Welcome, Merchant {user.name}</h1>
          </header>
          <main>
            <ul>
              <Link>
                <li>Display all orders</li>
              </Link>
              <Link>
                <li>Add Products</li>
              </Link>
              <button
                onClick={() =>
                  dispatch(
                    logoutWithRedirect({
                      token: access_token,
                      redirectCallback: () => history.push("/"),
                    })
                  )
                }
              >
                <li>Logout</li>
              </button>
            </ul>
          </main>
        </React.Fragment>
      )}
      {user && user.role.name === "super_admin_user" && (
        <div>I am super admin user</div>
      )}
    </div>
    // <Switch>
    //   <Route exact path={path} render={() => <Home />} />
    //   <Route path={`${path}/categories`}>
    //     <Category />
    //   </Route>
    //   <Route path={`${path}/products`}>
    //     <Product />
    //   </Route>
    // </Switch>
  );
}

export default Admin;
