import axios from "axios";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Button, Form, Modal } from "semantic-ui-react";
import { selectConfigId } from "../../../slices/configMenuSlice";
import ChooseParentCategory from "../CreateCategory/ChooseParentCategory";

const fetchCategory = (id) => {
  return axios
    .get(`/categories/${id}`)
    .then((response) => response.data.data)
    .catch((error) => error);
};

//eslint-disable-next-line
export default () => {
  const [name, setName] = useState("");
  const [parentId, setParentId] = useState();
  const configId = useSelector(selectConfigId);

  useEffect(() => {
    console.log(configId);
    if (configId)
      fetchCategory(configId)
        .then((category) => {
          console.log(category);
          setName(category.name);
          setParentId(category.parent_id);
        })
        .catch((error) => console.log(error));
    //eslint-disable-next-line
  }, []);

  const submitHandler = () => {
    axios
      .patch(`/categories/${configId}`, {
        name: name,
        parent_id: parentId,
      })
      .then(() => window.location.reload())
      .catch((error) => console.error(error));
  };

  return (
    <>
      <Modal.Header>Edit Category</Modal.Header>
      <Modal.Content>
        <Form onSubmit={submitHandler}>
          <Form.Field>
            <Form.Input
              label="Name"
              value={name}
              onChange={(e) => setName(e.target.value)}
              placeholder="Category Name"
            />
          </Form.Field>
          {parentId && (
            <Form.Field>
              <label>Parent Category: </label>
              <ChooseParentCategory
                default={parentId}
                onChange={(e, { value }) => setParentId(value)}
              />
            </Form.Field>
          )}
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button type="submit" positive>
          Update
        </Button>
      </Modal.Actions>
    </>
  );
};
