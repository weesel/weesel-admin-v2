import axios from "axios";
import React from "react";
import { useSelector } from "react-redux";
import { Button, Modal } from "semantic-ui-react";
import { selectConfigId } from "../../../slices/configMenuSlice";
function DeleteCategory() {
  const configId = useSelector(selectConfigId);

  const deleteCategory = async () => {
    axios
      .delete(`/categories/${configId}`)
      .then(() => window.location.reload())
      .catch((error) => console.log(error));
  };

  return (
    <>
      <Modal.Header>Delete Category</Modal.Header>
      <Modal.Content>
        <p>
          Are you sure you want to delete this category? (All Children will also
          be removed)
        </p>
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={() => deleteCategory()} positive>
          Confirm Delete
        </Button>
      </Modal.Actions>
    </>
  );
}

export default DeleteCategory;
