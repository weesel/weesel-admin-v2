import axios from "axios";
import { useEffect, useState } from "react";
import { Grid } from "semantic-ui-react";
import CreateCategory from "./CreateCategory/CreateCategory";

import CategoryTree from "../../components/TreeViewer";
import FAB from "../../components/FAB";
import EditCategory from "./EditCategory/EditCategory";
import { useSelector } from "react-redux";
import { selectRefreshCounter } from "../../slices/categorySlice";
import nProgress from "nprogress";
import DeleteCategory from "./DeleteCategory.js/DeleteCategory";

const fetchCategories = async () => {
  return axios
    .get("/categories")
    .then((response) => response.data.data)
    .catch((error) => error);
};

const deleteCategories = async (deleteId) => {
  return axios
    .get("/categories")
    .then((response) => response.data.data)
    .catch((error) => error);
};

//eslint-disable-next-line
export default () => {
  const [categories, setCategories] = useState();
  // create a refresh counter that changes
  const refreshCounter = useSelector(selectRefreshCounter);
  // create a refresh function that increments refresh counter
  useEffect(() => {
    // use set timeout for 1 second
    // start loading
    fetchCategories()
      .then((categories) => {
        // clear the timeout
        // set loading to false
        setCategories(categories);
      })
      .finally(nProgress.isStarted() && nProgress.done())
      .catch((error) => console.log(error));
  }, [refreshCounter]);
  return (
    <>
      <Grid className="mt-0" columns={2} celled>
        <Grid.Column width="10">
          <CreateCategory />
        </Grid.Column>
        <Grid.Column width="6">
          <CategoryTree
            name="Category"
            children={categories}
            Edit={<EditCategory />}
            Delete={<DeleteCategory />}
          />
        </Grid.Column>
      </Grid>
      <FAB icon="home" />
    </>
  );
};
