import React, { useEffect, useState } from "react";
import { Menu } from "semantic-ui-react";

export default () => {
  const [activeItem, setActiveItem] = useState("/admin/categories/add");
  useEffect(() => {
    const currentPath = window.location.href.split("http://localhost:3000")[1];
    setActiveItem(currentPath);
  }, []);

  return (
    <Menu tabular>
      <Menu.Item
        name="add"
        active={activeItem.search("add") !== -1}
        href="/admin/categories/add"
      />
      <Menu.Item
        name="edit"
        active={activeItem.search("edit") !== -1}
        href="/admin/categories/edit"
      />
    </Menu>
  );
};
