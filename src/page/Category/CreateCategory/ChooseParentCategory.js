import axios from "axios";
import React, { useEffect, useState } from "react";
import { Form } from "semantic-ui-react";
import { semanticUI__optionsFormatter } from "utils/constants";
const fetchParentCategories = () => {
  return axios
    .get("/categories?show=parent_only")
    .then((response) => response.data.data)
    .catch((error) => error);
};
function ChooseParentCategory(props) {
  const [parentCategories, setParentCategories] = useState();
  useEffect(() => {
    fetchParentCategories()
      .then((parentCategories) =>
        setParentCategories(
          semanticUI__optionsFormatter("name", "name", "id", parentCategories)
        )
      )
      .catch((error) => console.log(error));
  }, []);
  return (
    <Form.Dropdown
      value={props.default}
      placeholder="Parent Categories"
      fluid
      search
      selection
      onChange={props.onChange}
      options={parentCategories || []}
    />
  );
}
export default ChooseParentCategory;
