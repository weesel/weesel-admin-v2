import axios from "axios";
import nprogress from "nprogress";
import React, { useRef, useState } from "react";
import {
  Button,
  Container,
  Divider,
  Form,
  Header,
  Icon,
} from "semantic-ui-react";
import ChooseParentCategory from "./ChooseParentCategory";
import "css/nprogress.css";
import { refreshCategory } from "../../../slices/categorySlice";
import { useDispatch } from "react-redux";

function CreateCategory() {
  const [isSubcategory, setIsSubcategory] = useState(false);
  const [name, setName] = useState("");
  // const [description, setDescription] = useState("");
  const [parentID, setParentID] = useState(null);
  const [image, setImage] = useState("");
  const dispatch = useDispatch();

  const toggleIsSubCategory = () => {
    setIsSubcategory((prevState) => !prevState);
    setParentID(null);
  };

  const fileInput = useRef();
  const resetForm = () => {
    setName("");
    setParentID(null);
    setIsSubcategory(false);
    setImage(null);
    fileInput.current.value = "";
  };

  const submitHandler = () => {
    const formData = new FormData();
    formData.append("name", name);
    // formData.append("description", description);
    if (image) formData.append("image", image);
    if (parentID) formData.append("parent_id", parentID);
    nprogress.start();
    axios
      .post("/categories", formData)
      .then(() => {
        // increments the refresh counter using refresh function pass as props
        dispatch(refreshCategory());
        resetForm();
      })
      .catch((error) => console.log(error));
  };

  return (
    <Container>
      <Divider horizontal>
        <Header as="h4">
          <Icon name="plus cart" />
          Create Category
        </Header>
      </Divider>
      <Form onSubmit={submitHandler}>
        <Form.Field>
          <Form.Input
            value={name}
            onChange={(e) => setName(e.target.value)}
            label="Name"
            placeholder="Category Name"
          />
        </Form.Field>
        <Form.Field>
          <label>Image</label>
          <input
            type="file"
            name="image"
            ref={fileInput}
            onChange={(e) => setImage(...e.target.files)}
          />
        </Form.Field>
        <Form.Field>
          <Form.Checkbox
            onChange={toggleIsSubCategory}
            checked={isSubcategory}
            label="This category is a subcategory."
          />
        </Form.Field>
        {isSubcategory === true && (
          <Form.Field>
            <ChooseParentCategory
              onChange={(e, { value }) => setParentID(value)}
            />
          </Form.Field>
        )}
        <Button type="submit">Submit</Button>
      </Form>
    </Container>
  );
}

export default CreateCategory;
