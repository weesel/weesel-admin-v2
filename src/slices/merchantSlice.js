import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  orders: [],
  merchantRefreshCounter: 0,
};

export const merchantSlice = createSlice({
  name: "merchant",
  initialState,
  reducers: {
    setMerchantOrders: (state, newState) => {
      state.orders = newState.payload;
    },
    refreshMerchantCounter: (state) => {
      state.merchantRefreshCounter = state.merchantRefreshCounter + 1;
    },
  },
});

export const { setMerchantOrders, refreshMerchantCounter } =
  merchantSlice.actions;

export const selectMerchantOrders = (state) => state.merchant.orders;
export const selectMerchantCounter = (state) =>
  state.merchant.merchantRefreshCounter;

export default merchantSlice.reducer;
