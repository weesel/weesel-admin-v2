import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  refreshCounter: 0,
};

export const categorySlice = createSlice({
  name: "category",
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    refreshCategory: (state) => {
      state.refreshCounter++;
    },
  },
});

export const { refreshCategory } = categorySlice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state: RootState) => state.counter.value)`
export const selectRefreshCounter = (state) => state.category.refreshCounter;

export default categorySlice.reducer;
