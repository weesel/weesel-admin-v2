import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  configId: null,
  openEdit: false,
  openDelete: false,
};

export const configMenuSlice = createSlice({
  name: "configMenu",
  initialState,
  reducers: {
    setConfigId: (state, newState) => {
      state.configId = newState.payload;
    },
    setOpenEdit: (state, newState) => {
      state.openEdit = newState.payload;
    },
    setOpenDelete: (state, newState) => {
      state.openDelete = newState.payload;
    },
  },
});

export const { setConfigId, setOpenDelete, setOpenEdit } =
  configMenuSlice.actions;

export const selectConfigId = (state) => state.configMenu.configId;
export const selectOpenEdit = (state) => state.configMenu.openEdit;
export const selectOpenDelete = (state) => state.configMenu.openDelete;

export default configMenuSlice.reducer;
