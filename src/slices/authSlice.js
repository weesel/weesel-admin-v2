import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {
  fetchAuthenticatedUser,
  login,
  logout,
  removeAccessTokenFromStorage,
  setAccessTokenToStorage,
} from "utils/constants";

const initialState = {
  access_token: JSON.parse(localStorage.getItem("admin_access_token")) || null,
  remember_me: false,
  user: null,
  errorMessage: {
    header: "",
    content: "",
  },
};

export const checkAuth = createAsyncThunk("auth/checkAuth", async (token) => {
  const response = await fetchAuthenticatedUser(token);
  return response;
});

export const loginWithRedirect = createAsyncThunk(
  "auth/login",
  async (payload, { rejectWithValue }) => {
    try {
      const response = await login(payload.loginCredentials);
      return {
        user: response.user,
        access_token: response.access_token,
        redirectCallback: payload.redirectCallback,
      };
    } catch (error) {
      return rejectWithValue(error.response.status);
    }
  }
);

export const logoutWithRedirect = createAsyncThunk(
  "auth/logout",
  async (payload) => {
    return logout(payload.token).then((response) => {
      return {
        redirectCallback: payload.redirectCallback,
      };
    });
  }
);

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    clearErrorMessage: (state) => {
      state.errorMessage = {
        ...state.errorMessage,
        header: "",
        content: "",
      };
    },
  },
  extraReducers: {
    [checkAuth.rejected]: (state, _action) => {
      state.user = null;
    },
    [checkAuth.fulfilled]: (state, action) => {
      state.user = action.payload.user;
    },
    [loginWithRedirect.fulfilled]: (state, action) => {
      setAccessTokenToStorage(action.payload.access_token);
      action.payload.redirectCallback();
    },
    [loginWithRedirect.rejected]: (state, action) => {
      if (action.payload === 403) {
        state.errorMessage = {
          ...state.errorMessage,
          header: "Unauthorized action",
          content: "Please enter the correct login credentials",
        };
      } else if (action.payload === 500) {
        state.errorMessage = {
          ...state.errorMessage,
          header: "Unable to login",
          content: "Please enter the correct login credentials",
        };
      }
    },
    [logoutWithRedirect.fulfilled]: (_, action) => {
      removeAccessTokenFromStorage();
      action.payload.redirectCallback();
    },
  },
});

export const { clearErrorMessage } = authSlice.actions;

export const selectAccessToken = (state) => state.auth.access_token;
export const selectUser = (state) => state.auth.user;
export const selectAuthErrorMessage = (state) => state.auth.errorMessage;

export default authSlice.reducer;
