import { configureStore } from "@reduxjs/toolkit";
import configMenuReducer from "./slices/configMenuSlice";
import categoryReducer from "./slices/categorySlice";
import ProductReducer from "./slices/productSlice";
import authReducer from "slices/authSlice";
import merchantReducer from "slices/merchantSlice";

export const store = configureStore({
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
  reducer: {
    configMenu: configMenuReducer,
    category: categoryReducer,
    product: ProductReducer,
    auth: authReducer,
    merchant: merchantReducer,
  },
});
