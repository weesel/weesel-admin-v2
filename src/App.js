import React, { useEffect } from "react";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";

// import AdminMenu from "./components/AdminMenu";
import Admin from "./page/Admin";
import Login from "./page/Login";
import { selectUser, checkAuth, selectAccessToken } from "slices/authSlice";
import { useSelector, useDispatch } from "react-redux";
import Merchant from "page/Merchant";
import Product from "page/Product";
import Order from "page/Order";
import Nav from "components/Nav";

function App() {
  const access_token = useSelector(selectAccessToken);
  const user = useSelector(selectUser);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(checkAuth(access_token));
  }, [dispatch]);
  return (
    <div className="App">
      <BrowserRouter forceRefresh={true}>
        {user && <Nav />}
        <Switch>
          <Route exact path="/">
            {user ? <Redirect to="/merchant/dashboard" /> : <Login />}
          </Route>
          <Route path="/merchant/dashboard">
            <Merchant />
          </Route>
          <Route path="/merchant/order">
            <Order />
          </Route>
          <Route path="/categories/:id/products">
            <Product />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
