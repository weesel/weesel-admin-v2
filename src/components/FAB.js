import React from "react";
import { Button } from "semantic-ui-react";

const scrollToTop = () => {
  window.scrollTo(0, 0);
};

function FAB({ icon }) {
  return (
    <Button onClick={scrollToTop} className="fab-custom" circular icon={icon} />
  );
}

export default FAB;
