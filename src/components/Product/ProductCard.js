import React from "react";
import "css/ProductCard.css";

function ProductCard({ image, title, rating, price }) {
  return (
    <div className="product-card">
      <div className="product-card-image">
        <img src={image} alt="product" />
      </div>
      <div className="product-card-title">
        <span>{title}</span>
      </div>
      <div className="product-card-extra">
        <div className="product-card-ratings">
          {Array.from(Array(rating).keys()).map((_, i) => (
            <span key={`star-${i}`} className="fa fa-stack">
              <i className="fa fa-star fa-stack-2x"></i>
            </span>
          ))}
          {Array.from(Array(5 - rating).keys()).map((_, i) => (
            <span key={`star-o-${i}`} className="fa fa-stack">
              <i className="fa fa-star-o fa-stack-2x"></i>
            </span>
          ))}
        </div>
        <div className="product-card-price">
          <span>${price}</span>
        </div>
      </div>
    </div>
  );
}

export default ProductCard;
