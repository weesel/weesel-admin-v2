import React, { useEffect, useState } from "react";
import { Menu } from "semantic-ui-react";
import weeselLogo from "images/Asset 1weesel.svg";

function AdminMenu() {
  // const [activeItem, setActiveItem] = useState("/admin");

  // useEffect(() => {
  //   const currentPath = window.location.href.split(BASE_URL)[1];
  //   setActiveItem(currentPath);
  // }, []);

  return (
    <Menu className="mb-0" stackable>
      <Menu.Item>
        <img alt="weesel-logo" src={weeselLogo} />
      </Menu.Item>
      {/* <Menu.Item name="home" active={activeItem === "/admin"} href="/admin">
        Home
      </Menu.Item> */}

      <Menu.Item
        name="categories"
        // active={activeItem.search("categories") !== -1}
        href="/admin/categories"
      >
        Category
      </Menu.Item>

      <Menu.Item
        name="products"
        // active={activeItem.search("products") !== -1}
        href="/admin/products"
      >
        Product
      </Menu.Item>
    </Menu>
  );
}
export default AdminMenu;
