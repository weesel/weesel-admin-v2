import React from "react";
import { Icon } from "semantic-ui-react";
import ConfigMenu from "./ConfigMenu/ConfigMenu";

const toggleExpandIcon = (element) => {
  if (element.classList.contains("down")) {
    element.classList.remove("down");
    element.classList.add("right");
  } else {
    element.classList.remove("right");
    element.classList.add("down");
  }
};

const toggleChildren = (e) => {
  // might change later
  toggleExpandIcon(e.target);
  // i ->  div -> nextSibling
  e.target.parentElement.nextSibling?.classList.toggle("hide");
};

//eslint-disable-next-line
export default ({ name, children, Edit, Delete }) => {
  const createTree = (children) => {
    return (
      <ul>
        {children.map((child) => (
          <li key={child.id}>
            <div
              className="row"
              style={{
                borderBottom: "1px solid grey",
                marginTop: "1rem",
                marginBottom: "1rem",
              }}
            >
              <Icon
                className="clickable"
                onClick={toggleChildren}
                name="caret down"
              />
              {child.name}
              <ConfigMenu
                id={child.id}
                editComponent={Edit}
                deleteComponent={Delete}
              />
            </div>
            {child.children.length > 0 && (
              <ul>
                {child.children.map((grandChild) => (
                  <li className="row" key={grandChild.id}>
                    {grandChild.name}
                    <ConfigMenu
                      id={grandChild.id}
                      editComponent={Edit}
                      deleteComponent={Delete}
                    />
                  </li>
                ))}
              </ul>
            )}
          </li>
        ))}
      </ul>
    );
  };
  return (
    <>
      {children && children.length > 0 ? (
        createTree(children)
      ) : (
        <span>No {name} Exist</span>
      )}
    </>
  );
};
