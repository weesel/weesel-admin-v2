import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "css/Nav.css";
import { fetchCategoriesWithChildren } from "utils/constants";

function Nav() {
  const [categories, setCategories] = useState([]);
  const [queryProductName, setQueryProductName] = useState("");
  const [queryCategory, setQueryCategory] = useState("all");
  useEffect(() => {
    fetchCategoriesWithChildren()
      .then((categories) => setCategories(categories))
      .catch((error) => console.log(error));
  }, []);
  return (
    <nav className="nav">
      <Link to="/">
        <img
          style={{ margin: "0.5rem" }}
          src="https://demo.templatetrip.com/Opencart/OPC02/OPC040/OPC05/image/catalog/logo.png"
          alt="logo.png"
        />
      </Link>
    </nav>
  );
}

export default Nav;
