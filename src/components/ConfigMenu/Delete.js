import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Button, Modal } from "semantic-ui-react";
import { setConfigId } from "../../slices/configMenuSlice";

function Delete({ id, children }) {
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();
  const openModal = () => {
    setOpen(true);
    dispatch(setConfigId(id));
  };
  const closeModal = () => {
    setOpen(false);
    dispatch(setConfigId(null));
  };
  return (
    <>
      <Modal
        trigger={
          <Button primary size="tiny" secondary>
            Delete
          </Button>
        }
        onClose={closeModal}
        onOpen={openModal}
        open={open}
      >
        {children}
      </Modal>
    </>
  );
}

export default Delete;
