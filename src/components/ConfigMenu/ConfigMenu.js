import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "semantic-ui-react";
import {
  selectConfigId,
  setConfigId,
  setOpenDelete,
  setOpenEdit,
} from "../../slices/configMenuSlice";
import Delete from "./Delete";
import Edit from "./Edit";

function ConfigMenu({ id, editComponent, deleteComponent }) {
  // const dispatch = useDispatch();
  // const configId = useSelector(selectConfigId);
  // const openEditMenu = () => {
  //   // dispatch(setOpenEdit(true));

  //   //! error
  //   dispatch(setConfigId(id));
  // };
  // const openDeleteMenu = () => {
  //   // dispatch(setOpenDelete(true));

  //   //! error
  //   dispatch(setConfigId(id));
  // };
  return (
    <div
      style={{
        marginLeft: "auto",
        marginBottom: "0.5rem",
      }}
    >
      <Edit children={editComponent} id={id} />
      <Delete children={deleteComponent} id={id} />
    </div>
  );
}

export default ConfigMenu;
