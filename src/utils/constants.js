import axios from "axios";

/**
 * @params key|string, text|string, value|string
 * @returns array
 */
export const semanticUI__optionsFormatter = (key, text, value, rawData) => {
  return rawData.reduce((accumulator, data) => {
    return (accumulator = [
      ...accumulator,
      {
        key: data[key],
        text: data[text],
        value: data[value],
      },
    ]);
  }, []);
};

export const removeAccessTokenFromStorage = () =>
  localStorage.removeItem("admin_access_token");
export const setAccessTokenToStorage = (token) =>
  localStorage.setItem("admin_access_token", JSON.stringify(token));
export const getAccessTokenFromStorage = () => {
  return JSON.parse(localStorage.getItem("admin_access_token"));
};

export const login = async (payload) => {
  try {
    const response = await axios.post("/auth/admin/login", payload);
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const logout = async (token) => {
  try {
    const response = await axios.post("/auth/admin/logout", null, {
      headers: { Authorization: `Bearer ${token}` },
    });
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const fetchAuthenticatedUser = async (token) => {
  try {
    const response = await axios.get("/auth/me", {
      headers: { Authorization: `Bearer ${token}` },
    });
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const fetchMerchantOrders = async (token, seller_id) => {
  try {
    const response = await axios.get(`/sellers/${seller_id}/orders`, {
      headers: { Authorization: `Bearer ${token}` },
    });
    return response.data.data;
  } catch (error) {
    throw error;
  }
};

export const fetchMerchantOrdersByDate = async (token, seller_id, date) => {
  try {
    const response = await axios.get(`/sellers/${seller_id}/orders`, {
      headers: { Authorization: `Bearer ${token}` },
      params: {
        date: date,
      },
    });
    return response.data.data;
  } catch (error) {
    throw error;
  }
};

export const fetchCategoriesWithChildren = async () => {
  return axios
    .get("/categories", {
      params: {
        include: "children",
      },
    })
    .then((response) => response.data.data)
    .catch((error) => error);
};

export const fetchCategoryWithChildren = async (id) => {
  return axios
    .get(`/categories/${id}`, {
      params: {
        include: "children",
      },
    })
    .then((response) => response.data.data)
    .catch((error) => error);
};

export const capitalizeFirstLetter = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};
